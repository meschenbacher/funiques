package main

import (
	"crypto/sha512"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

// pathFileInfo contains path along os.FileInfo (which does not carry the path itself)
type pathFileInfo struct {
	path string
	info os.FileInfo
}

// map file sizes to list of pathFileInfo structs
var size2fileinfos = make(map[uint64][]pathFileInfo)

func main() {
	for _, dir := range os.Args[1:] {
		err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.IsDir() {
				return nil
			}
			size2fileinfos[uint64(info.Size())] = append(size2fileinfos[uint64(info.Size())], pathFileInfo{path: path, info: info})
			return nil
		})
		if err != nil {
			log.Fatalf("Error walking %s: %v", dir, err)
		}
	}
	for _, infos := range size2fileinfos {
		// fast case, only one file with this exact file size
		if len(infos) == 1 {
			fmt.Println(infos[0].path)
			continue
		}

		// slow case
		// walk over all files with the same file size and calculate their sha512sums
		m := make(map[[64]byte][]pathFileInfo)
		for _, pfi := range infos {
			file, err := os.Open(pfi.path)
			if err != nil {
				log.Fatalf("Error opening %s: %v", pfi.path, err)
			}
			hash := sha512.New()
			if _, err = io.Copy(hash, file); err != nil {
				log.Fatalf("Error reading %s: %v", pfi.path, err)
			}
			file.Close()
			var sum [64]byte
			copy(sum[:], hash.Sum(nil))
			m[sum] = append(m[sum], pathFileInfo{path: pfi.path, info: pfi.info})
		}

		// print all hash map values where only one file produces this hash
		for _, infos := range m {
			if len(infos) == 1 {
				fmt.Println(infos[0].path)
			}
		}
	}
}
