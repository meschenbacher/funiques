# funiques

`funiques` traverses all supplied directories and lists all unique files.

Inspired by `fdupes` but produces the negated output (in a sense).

# Build and run

    go build
    ./funiques somefolder1 somefolder2

# Achieve the same result with alternate UNIX tooling

    jdupes -d -r somefolder1 somefolder2
